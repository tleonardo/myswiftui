//
//  ContentView.swift
//  MySwiftUI
//
//  Created by Timotius Leonardo Lianoto on 19/05/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .font(.largeTitle)
            .fontWeight(.bold)
            .foregroundColor(Color.blue)
            .multilineTextAlignment(.center)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
